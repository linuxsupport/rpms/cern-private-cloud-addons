# cern-private-cloud-addons
* This package configures the following on Virtual Machines in the CERN Cloud Infrastructure service
    * OpenAFS
    * Kerberos
    * NTP
    * LANDB ownership accounts + sudo privileges
