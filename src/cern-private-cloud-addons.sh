#!/bin/bash

# don't run if puppet configuration in progress
# this is temporary hack (Dec 2016)
[ -f /etc/puppet/puppet.conf ] && [ `grep -c it-puppet-masters /etc/puppet/puppet.conf` -ne 0 ] && exit 0
# adding 'temporary' hack for puppet 4 too (Sep 2017)
[ -f /etc/puppetlabs/puppet/puppet.conf ] && [ `grep -c it-puppet-masters /etc/puppetlabs/puppet/puppet.conf` -ne 0 ] && exit 0

# Run only once

statefile=/var/lib/cern-private-cloud-addons/state
[ -e ${statefile} ] && exit 0

# For Kerberos Keytab creation we need the hostname to exist in DNS - this may take up to 15 minutes

for i in `seq 1 90`
do
    shorthost=`/bin/hostname -s 2>/dev/null`
    [ -n "${shorthost}" ] && break
    /bin/echo `date` "Hostname not found in DNS, retrying..."
    sleep 10
done

if [ -z "${shorthost}" ]
then
    /bin/echo "No hostname found for this machine, giving up"
    exit 1
fi

# Create accounts, mail forwarding, etc.

/usr/sbin/cern-config-users --setup-all
[ $? -eq 0 ] && /bin/touch ${statefile}

# temp. hack to get profiles linked correctly.
/usr/bin/yum -y update lcm-profile 
/usr/bin/yum -y reinstall lcm-profile

# Configure the machine

/usr/sbin/lcm --configure --all

# and add afsclt

/usr/sbin/lcm --configure afsclt
/sbin/chkconfig levels 345 afs on
/sbin/service afs start


exit 0

