%{!?dist: %define dist .el8.cern}

Name: cern-private-cloud-addons
Version: 0.8
Release: 3%{?dist}
Summary: '%{name}' configures Virtual Machines in the CERN Cloud Infrastructure service
Group: Applications/System
Source: %{name}-%{version}.tgz
License: GPL
Vendor: CERN
Packager: Linux.Support@cern.ch
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch

%if 0%{?rhel} >= 7
Requires: locmap
Requires: systemd
#locmap
%else
Requires: cern-config-users
Requires: lcm-profile
Requires: lcm
Requires: chkconfig
%endif

%description 
'%{name}' configures SLC/CC Virtual Machines in the CERN Cloud Infrastructure service:
  * it creates the necessary accounts + sudo privileges
  * it sets us root e-mail forwarding and .k5login file
  * it adds the printers in the buildings where the users have their offices
  * it creates a Kerberos5 keytab /etc/krb5.keytab

Provides 'cern-recommended-setup' script allowing to align installed package sets with
standard desktop / server installations.

%prep
%setup -q

%build

%install

mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
%if 0%{?rhel} >= 7
install -m 755 cern-private-cloud-addons.sh-cc7 $RPM_BUILD_ROOT/%{_sbindir}/cern-private-cloud-addons
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system/
install -m 755 cern-private-cloud-addons.service $RPM_BUILD_ROOT/usr/lib/systemd/system/cern-private-cloud-addons.service
%else
install -m 755 cern-private-cloud-addons.sh $RPM_BUILD_ROOT/%{_sbindir}/cern-private-cloud-addons
mkdir -p $RPM_BUILD_ROOT/%{_initrddir}
install -m 755 zz_cern-private-cloud-addons   $RPM_BUILD_ROOT/%{_initrddir}/zz_cern-private-cloud-addons
%endif
install -m 755 cern-recommended-setup $RPM_BUILD_ROOT/%{_sbindir}/cern-recommended-setup
mkdir -p $RPM_BUILD_ROOT/%{_localstatedir}/lib/cern-private-cloud-addons

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root)
%{_sbindir}/cern-private-cloud-addons
%if 0%{?rhel} >= 7
/usr/lib/systemd/system/cern-private-cloud-addons.service
%else
%{_initrddir}/zz_cern-private-cloud-addons
%endif
%{_sbindir}/cern-recommended-setup
%{_localstatedir}/lib/cern-private-cloud-addons

%post
%if 0%{?rhel} >= 7
 /bin/systemctl enable cern-private-cloud-addons.service || :
%else
 /sbin/chkconfig --add zz_cern-private-cloud-addons || :
%endif
%preun
if [ "$1" = 0 ] ; then
%if 0%{?rhel} >= 7
  /bin/systemctl --no-reload disable cern-private-cloud-addons.service || :
%else
  /sbin/chkconfig --del zz_cern-private-cloud-addons
%endif
fi

%postun

%changelog
* Mon Jan 27 2020 Ben Morrice <ben.morrice@cern.ch> - 0.8-3
- package for c8

* Mon Nov 26 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.8-2
- Remove requires on cern-config-users for el7

* Tue May 15 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.8-1
- use only locmap on CC7

* Thu Sep 14 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.7-1
- prevent running if puppet 4 configurqation in progress.

* Fri Jan 13 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.6-1
- use locmap on CC7.3

* Tue Dec 13 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.5.1-1
- systemd startup on 7.
- fix 5/6 to startup AFTER cloud-init.
- do not start if puppet is coming.
- added cern-recommended-setup script

* Wed Dec 07 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 0.2-1
- fix dependencies/scripts

* Mon Sep 16 2013 Jan van Eldik <Jan.van.Eldik@cern.ch> - 0.1-2
- "Requires: krb5-workstation" to fix issue in "SLC6 CERN Server [130922]" images

* Mon Sep 16 2013 Jan van Eldik <Jan.van.Eldik@cern.ch> - 0.1-1
- initial build
